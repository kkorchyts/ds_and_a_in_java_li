package com.kkorchyts.javapractice.datastructureandalgoritms;

import com.kkorchyts.javapractice.datastructureandalgoritms.sortings.AlgoritmsAnalysis;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestAlgoritmsAnalysis {

    @Test
    public void BubbleSortCheckNormalArray() {
        int[] sourceArray = new int[]{8, 3, 2, 9, -1};
        int[] expectedResultArray = new int[]{-1, 2, 3, 8, 9};
        int[] resultArray = AlgoritmsAnalysis.bubbleSort(sourceArray);
        Assertions.assertArrayEquals(expectedResultArray, resultArray);
    }

    @Test
    public void CheckBubbleSortCheckEmptyArray() {
        int[] sourceArray = new int[]{};
        int[] expectedResultArray = new int[]{};
        int[] resultArray = AlgoritmsAnalysis.bubbleSort(sourceArray);
        Assertions.assertArrayEquals(expectedResultArray, resultArray);
    }

    @Test
    public void BubbleSortCheckArrayWithSameNumbers() {
        int[] sourceArray = new int[]{-1, -1, -1, -1, -1};
        int[] expectedResultArray = new int[]{-1, -1, -1, -1, -1};
        int[] resultArray = AlgoritmsAnalysis.bubbleSort(sourceArray);
        Assertions.assertArrayEquals(expectedResultArray, resultArray);
    }

    @Test
    public void BubbleSortCheckNullArray() {
        int[] sourceArray = null;
        Throwable throwable = Assertions.assertThrows(NullPointerException.class, () -> {
            AlgoritmsAnalysis.bubbleSort(sourceArray);
        });
        Assertions.assertNotNull(throwable);
    }
}
