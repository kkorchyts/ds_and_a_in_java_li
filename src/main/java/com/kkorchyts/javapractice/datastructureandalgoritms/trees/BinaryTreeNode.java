package com.kkorchyts.javapractice.datastructureandalgoritms.trees;

import java.awt.print.Pageable;
import java.util.Optional;

public class BinaryTreeNode<T extends Comparable<T>> {
    private final T data;
    private BinaryTreeNode<T> leftChild;
    private BinaryTreeNode<T> rightChild;
    private boolean isDeleted = false;

    public BinaryTreeNode(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public Optional<BinaryTreeNode<T>> find(T data) {
        if (data.equals(this.data) && !this.isDeleted) {
            return Optional.of(this);
        }

        if (data.compareTo(this.data) < 0 && leftChild != null) {
            return leftChild.find(data);
        }

        if (data.compareTo(this.data) > 0 && rightChild != null) {
            return rightChild.find(data);
        }

        return Optional.empty();
    }

    public void insert(T data) {
        if (data.compareTo(this.data) < 0) {
            if (leftChild != null) {
                leftChild.insert(data);
            } else {
                leftChild = new BinaryTreeNode<>(data);
            }
        }

        if (data.compareTo(this.data) >= 0) {
            if (rightChild != null) {
                rightChild.insert(data);
            } else {
                rightChild = new BinaryTreeNode<>(data);
            }
        }
    }

    public Optional<BinaryTreeNode<T>> getSmallest() {
        // TODO: capture the case with deleted node.
        if (rightChild != null) {
            return rightChild.getSmallest();
        } else {
            return Optional.of(this);
        }
    }

    public void delete() {
        isDeleted = true;
    }

    public boolean isDeleted () {
        return isDeleted;
    }

    public BinaryTreeNode<T> getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(BinaryTreeNode<T> leftChild) {
        this.leftChild = leftChild;
    }

    public BinaryTreeNode<T> getRightChild() {
        return rightChild;
    }

    public void setRightChild(BinaryTreeNode<T> rightChild) {
        this.rightChild = rightChild;
    }
}
