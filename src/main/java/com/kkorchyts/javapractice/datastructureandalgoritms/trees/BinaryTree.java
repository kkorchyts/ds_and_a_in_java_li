package com.kkorchyts.javapractice.datastructureandalgoritms.trees;

import java.util.Optional;

public class BinaryTree<T extends Comparable<T>> {
    private BinaryTreeNode<T> root;

    public void insert(T data){
        if (root == null) {
            root = new BinaryTreeNode<>(data);
        } else {
            root.insert(data);
        }
    }

    public Optional<BinaryTreeNode<T>> find(T data) {
        if (root != null) {
            return root.find(data);
        }

        return Optional.empty();
    }

    public Optional<BinaryTreeNode<T>> getSmallest() {
        // TODO: capture the case with deleted node.
        if (root == null) {
            return Optional.empty();
        }

        BinaryTreeNode<T> current = root;
        while (current.getLeftChild() != null) {
            current = current.getLeftChild();
        }
        return Optional.of(current);
    }

    public Optional<BinaryTreeNode<T>> getBiggest() {
        // TODO: capture the case with deleted node.
        if (root == null) {
            return Optional.empty();
        }

        BinaryTreeNode<T> current = root;
        while (current.getRightChild() != null) {
            current = current.getRightChild();
        }
        return Optional.of(current);
    }

    public void softDelete(T data){
        Optional<BinaryTreeNode<T>> treeNode = find(data);
        treeNode.ifPresent(BinaryTreeNode::delete);
    }

    public void delete(T data) {
        BinaryTreeNode<T> current = root;
        BinaryTreeNode<T> parent = root;
        boolean isLeftChild = false;

        if (current == null) {
            return;
        }

        while (current != null && current.getData().compareTo(data) != 0) {
            parent = current;

            if (data.compareTo(current.getData()) < 0) {
                current = current.getLeftChild();
                isLeftChild = true;
            } else {
                current = current.getRightChild();
                isLeftChild = false;
            }
        }

        if (current == null) {
            return;
        }

        if (current.getLeftChild() == null && current.getRightChild() == null) {
            if (current == root) {
                root = null;
            } else {
                if (isLeftChild) {
                    parent.setLeftChild(null);
                } else {
                    parent.setRightChild(null);
                }
            }
        } else {
            if (current.getRightChild() == null) {
                if (current == root) {
                    root = current.getLeftChild();
                } else {
                    if (isLeftChild) {
                        parent.setLeftChild(current.getLeftChild());
                    } else {
                        parent.setRightChild(current.getLeftChild());
                    }
                }
            } else {
                if (current.getLeftChild() == null) {
                    if (current == root) {
                        root = current.getRightChild();
                    } else {
                        if (isLeftChild) {
                            parent.setLeftChild(current.getRightChild());
                        } else {
                            parent.setRightChild(current.getRightChild());
                        }
                    }
                }
            }
        }
    }
}
