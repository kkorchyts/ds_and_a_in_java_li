package com.kkorchyts.javapractice.datastructureandalgoritms.recursive;

public class HanoiTower {
    public void move (int numberOfDiscs, char from, char to, char inter) {
        if (numberOfDiscs == 1) {
            System.out.println("Move Disc " + (numberOfDiscs) + " from '" + from + "' to '" + to + "'");
        } else {

            move(numberOfDiscs - 1, from, inter, to);
            System.out.println("Move Disc " + (numberOfDiscs) + " from '" + from + "' to '" + to + "'");
            move(numberOfDiscs - 1, inter, to, from);
        }
    }

    public static void main(String[] args) {
        HanoiTower ht = new HanoiTower();
        ht.move(3, 'A', 'C', 'B');
    }

}
