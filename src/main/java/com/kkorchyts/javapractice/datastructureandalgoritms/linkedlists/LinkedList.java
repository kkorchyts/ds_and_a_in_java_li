package com.kkorchyts.javapractice.datastructureandalgoritms.linkedlists;

import com.kkorchyts.javapractice.datastructureandalgoritms.linkedlists.exceptions.NodeNotFoundException;

import java.util.Optional;

public class LinkedList {
    private Node head;

    public void insertAtHead (int data) {
        Node newNode = new Node(data);
        newNode.setNextNode(this.head);
        this.head = newNode;
    }

    public int lenght() {
        int lenght = 0;
        Node current = this.head;

        while (current != null) {
            lenght++;
            current = current.getNextNode();
        }
        return lenght;
    }

    public void deleteAtStart() {
        if (this.head != null) {
            this.head = this.head.getNextNode();
        }
    }

    public void insert(int data) {
        // if LinkedList empty - insert into head
        Node newNode = new Node(data);
        if (this.head == null) {
            this.head = newNode;
            return;
        }

        // if the first greater than data replace head
        if (this.head.getData() >= newNode.getData()) {
            newNode.setNextNode(this.head);
            this.head = newNode;
            return;
        }

        Node current = this.head;

        while (current != null) {
            // if next node is - check it
            if (current.getNextNode() != null) {
                if (current.getNextNode().getData() >= newNode.getData()) {
                    newNode.setNextNode(current.getNextNode());
                    current.setNextNode(newNode);
                    return;
                }
                current = current.getNextNode();
            // if we reach tail - add our data into tail
            } else {
                current.setNextNode(newNode);
                return;
            }
        }
    }


    public Optional<Node> find(int data) {
        Node current = this.head;
        while (current != null) {
            if (current.getData() == data) {
                return Optional.of(current);
            }
            current = current.getNextNode();
        }
        return Optional.empty();
    }

    @Override
    public String toString() {
        String result = "{";
        Node current = this.head;
        while (current != null) {
            result += current.toString();
            current = current.getNextNode();
            if (current != null) {
                result += "; ";
            }
        }

        result += "}";
        return result;
    }
}
