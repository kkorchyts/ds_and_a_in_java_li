package com.kkorchyts.javapractice.datastructureandalgoritms.linkedlists;

public class DoubleLinkedList {
    private DoubleLinkedNode head;

    public void insertAtHead (int data) {
        DoubleLinkedNode newNode = new DoubleLinkedNode(data);
        if (this.head == null) {
            this.head = newNode;
        } else {
            newNode.setNextNode(this.head);
            this.head.setPreviousNode(newNode);
            this.head = newNode;
        }
    }

    @Override
    public String toString() {
        String result = "{";
        DoubleLinkedNode current = this.head;
        while (current != null) {
            result += current.toString();
            current = current.getNextNode();
            if (current != null) {
                result += "; ";
            }
        }

        result += "}";
        return result;
    }

}
