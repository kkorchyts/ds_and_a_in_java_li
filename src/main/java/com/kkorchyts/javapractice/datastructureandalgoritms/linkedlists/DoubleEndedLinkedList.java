package com.kkorchyts.javapractice.datastructureandalgoritms.linkedlists;

public class DoubleEndedLinkedList {
    private Node head;
    private Node tail;

    public void insertAtTail (int data) {
        Node newNode = new Node(data);
        if (head == null) {
            head = newNode;
        }

        if (tail != null) {
            this.tail.setNextNode(newNode);
        }
        this.tail = newNode;
    }

    @Override
    public String toString() {
        String result = "{";
        Node current = this.head;
        while (current != null) {
            result += current.toString();
            current = current.getNextNode();
            if (current != null) {
                result += "; ";
            }
        }

        result += "}";
        return result;
    }
}
