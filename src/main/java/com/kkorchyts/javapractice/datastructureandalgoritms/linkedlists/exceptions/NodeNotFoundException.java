package com.kkorchyts.javapractice.datastructureandalgoritms.linkedlists.exceptions;

public class NodeNotFoundException extends RuntimeException{
    public NodeNotFoundException(String message) {
        super(message);
    }
}
