package com.kkorchyts.javapractice.datastructureandalgoritms.sortings;

import java.util.Arrays;

public class AlgoritmsAnalysis {

    public static int[] bubbleSort(int[] arrayToSort) {
        for (int i = 0; i < arrayToSort.length - 1; i++) {
            for (int j = 0; j < arrayToSort.length - i - 1; j++) {
                if (arrayToSort[j] > arrayToSort[j + 1]) {
                    int tmp = arrayToSort[j];
                    arrayToSort[j] = arrayToSort[j + 1];
                    arrayToSort[j + 1] = tmp;
                }
            }
        }
        return arrayToSort;
    }

    public static int[] selectionSort(int[] arrayToSort) {
        for (int i = 0; i < arrayToSort.length - 1; i++) {
            int indexOfMin = i;
            for (int j = i + 1; j < arrayToSort.length; j++) {
                if (arrayToSort[indexOfMin] > arrayToSort[j]) {
                    indexOfMin = j;
                }
            }
            if (indexOfMin != i) {
                    int tmp = arrayToSort[indexOfMin];
                    arrayToSort[indexOfMin] = arrayToSort[i];
                    arrayToSort[i] = tmp;
            }
        }
        return arrayToSort;
    }

    public static int[] insertionSort(int[] arrayToSort) {
        for (int i = 1; i < arrayToSort.length; i++) {
            int currentNumber = arrayToSort[i];
            int j = i - 1;
            while (j >= 0 && arrayToSort[j] > currentNumber) {
                arrayToSort[j + 1] = arrayToSort[j];
                j--;
            }
            arrayToSort[j + 1] = currentNumber;
        }
        return arrayToSort;
    }




    public static void main(String[] args) {
        int[] arrayToBubbleSort = new int[]{8,6,4,2,1};
        System.out.println(Arrays.toString(bubbleSort(arrayToBubbleSort)));

        int[] arrayToSelectionSort = new int[]{8,6,4,9,1};
        System.out.println(Arrays.toString(selectionSort(arrayToSelectionSort)));

        int[] arrayToInsertionSort = new int[]{8,6,4,9,1};
        System.out.println(Arrays.toString(insertionSort(arrayToInsertionSort)));
    }


}
