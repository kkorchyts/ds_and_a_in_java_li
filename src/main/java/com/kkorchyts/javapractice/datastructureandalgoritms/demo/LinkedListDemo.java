package com.kkorchyts.javapractice.datastructureandalgoritms.demo;

import com.kkorchyts.javapractice.datastructureandalgoritms.linkedlists.LinkedList;
import com.kkorchyts.javapractice.datastructureandalgoritms.linkedlists.Node;
import com.kkorchyts.javapractice.datastructureandalgoritms.linkedlists.exceptions.NodeNotFoundException;

import java.util.Optional;

public class LinkedListDemo {
    public static void main(String[] args) {
        LinkedList list = new LinkedList();

        list.insert(20);
        list.insert(19);
        list.insert(12);
        list.insert(10);
        list.insert(5);
        list.insert(2);
        list.insert(1);
        list.insert(6);


        System.out.println(list);
        System.out.println("List lenght = " + list.lenght());

        list.deleteAtStart();
        System.out.println(list);
        System.out.println("List lenght = " + list.lenght());
        Optional<Node> searchNode = list.find(15);
        System.out.println(searchNode.orElse(null));

        searchNode = list.find(5);
        System.out.println(searchNode.orElse(null));

        list.insert(5);
        System.out.println(list);

    }
}
