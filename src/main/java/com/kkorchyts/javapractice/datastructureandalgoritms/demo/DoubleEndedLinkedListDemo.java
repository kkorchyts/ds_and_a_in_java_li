package com.kkorchyts.javapractice.datastructureandalgoritms.demo;

import com.kkorchyts.javapractice.datastructureandalgoritms.linkedlists.DoubleEndedLinkedList;

public class DoubleEndedLinkedListDemo {
    public static void main(String[] args) {
        DoubleEndedLinkedList dList = new DoubleEndedLinkedList();
        dList.insertAtTail(5);
        dList.insertAtTail(10);
        dList.insertAtTail(2);

        System.out.println(dList);
    }
}
