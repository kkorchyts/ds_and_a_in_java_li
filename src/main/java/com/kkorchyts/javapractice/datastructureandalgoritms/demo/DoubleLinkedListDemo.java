package com.kkorchyts.javapractice.datastructureandalgoritms.demo;

import com.kkorchyts.javapractice.datastructureandalgoritms.linkedlists.DoubleLinkedList;

public class DoubleLinkedListDemo {
    public static void main(String[] args) {
        DoubleLinkedList doubleLinkedList = new DoubleLinkedList();
        doubleLinkedList.insertAtHead(5);
        doubleLinkedList.insertAtHead(15);
        doubleLinkedList.insertAtHead(14);

        System.out.println(doubleLinkedList);
    }
}
